<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Looping PHP</h1>
    <?php
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";
        echo "LOOPING PERTAMA<br>";
        for($i=2; $i<=19; $i+=1){
            echo $i." -I Love PHP <br>";
        };
        echo "LOPPING KEDUA<br>";
        for($i=2; $i<=19; $i+=1){
            echo 21-$i." -I Love PHP <br>";
        };
        
        
        echo "<h3>Soal No 2 Looping Array Modulo</h3>";
        $numbers = [18, 45, 29, 61, 47, 34];
        echo "Array numbers : <br>";
        print_r($numbers);
        echo "<br>";
        echo "Hasil sisa dari Array Angka : <br>";
        foreach($numbers as $nilai){
            $rest[]=$nilai %=5;
        }
        print_r($rest);
        echo "<br>";

        echo "<h3> Soal No 3 Looping Asociative Array </h3>";
        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];
        echo "Hasil Associative Array : <br>";
        foreach($items as $key => $value){
            $baris = array(
                "Id" => $value[0],
                "Name" => $value[1],
                "Price" => $value[2],
                "Description" => $value[3],
                "Source" => $value[4]
            );
            print_r($baris);
            echo "<br>";
        }

        echo "<h3>Soal No 4 Asterix </h3>";
        for($i=1; $i<=5; $i++){
            for($j=1;$j<=$i;$j++){
                echo "*";
            }
            echo "<br>";
        };





    ?>


</body>
</html>