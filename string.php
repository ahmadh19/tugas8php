<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Contoh Soal</h2>
    
    <?php
        echo "<h3>Soal No 1</h3>";
        $kalimat1 ="PHP is never old";
        echo "Kalimat pertama : ".$kalimat1."<br>";
        echo "Panjang String : ".strlen($kalimat1)."<br>";
        echo "Jumlah Kata : ".str_word_count($kalimat1)."<br>";

        echo "<h3>Soal No 2</h3>";
        $string2 ="I love PHP";
        echo "Kalimat kedua : ".$string2."<br>";
        echo "Kata pertama : ".substr($string2,0,1)."<br>";
        echo "Kata kedua : ".substr($string2,2,5)."<br>";
        echo "Kata ketiga : ".substr($string2,6,9)."<br>";

        echo "<h3>Soal No 3</h3>";
        $string3 ="PHP is old but Good";
        echo "Kalimat ketiga ".$string3."<br>";
        echo "Ganti kata : ".str_replace("Good","Awesome",$string3)."<br>";

    ?>
</body>
</html>