<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih dengan Array</h1>
    <?php
        echo "<h3>Soal 1</h3>";
        $kids =["Mike","Dustin","Will","Lucas","Max","Eleve"];
        $adults =["Hopper","Nancy","Joyce","Jonathan","Murray"];
        print_r($kids);
        echo "<br>";
        print_r($adults);

        echo "<h3>Soal 2</h3>";
        echo "Total kids : ".count($kids);
        echo "<br> Terdiri atas :";
        echo "<ol>";
            echo "<li>".$kids[0]."</li>";
            echo "<li>".$kids[1]."</li>";
            echo "<li>".$kids[2]."</li>";
            echo "<li>".$kids[3]."</li>";
            echo "<li>".$kids[4]."</li>";
            echo "<li>".$kids[5]."</li>";
        echo "</ol>";
        echo "Total adults : ".count($adults);
        echo "<br> Terdiri atas :";
        echo "<ol>";
            echo "<li>".$adults[0]."</li>";
            echo "<li>".$adults[1]."</li>";
            echo "<li>".$adults[2]."</li>";
            echo "<li>".$adults[3]."</li>";
            echo "<li>".$adults[4]."</li>";
        echo "</ol>";


        echo "<h3> Soal 3 </h3>";
        $trainer = [
            ["Will Byers","12","Will the Wise","Alive"],
            ["Mike Wheler","12","Dugon Master","Alive"],
            ["Jim Hopper","43","Chief Hopper","Deceased"],
            ["Eleven","12","El","Alive"]
        ];

        echo "<pre>";
        print_r($trainer);
        echo "</pre>";




    ?>
</body>
</html>