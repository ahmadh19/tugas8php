<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Function PHP</h1>
    <?php
        echo "<h3> Soal No 1 Greetings </h3>";
        function greetings($nama){
            echo "Halo ".$nama." Selamat Datang di PKS Digital School <br>";
        }

        greetings("Bagas");
        greetings("Wahyu");
        greetings("Abdul");

        echo "<br>";
        echo "<h3> Soal No 2 Reverse String </h3>";
        function reverse($kata1){
            $panjangkata = strlen($kata1);
            $tampung = "";
            for($i=($panjangkata-1); $i>=0; $i--){
                $tampung .= $kata1[$i];
            }
            return $tampung;
        }

        function reverseString($kata2){
            $string=reverse($kata2);
            echo $string."<br>";
        }

        reverseString("abduh");
        reverseString("Digital School");
        reverseString("We Are PKS Digital School Developers");
        echo "<br>";

        echo "<br>";
        echo "<h3> Soal No 3 Palindrome </h3>";
        function palindrome($kata3){
            $tampung = "";
            for($i=(strlen($kata3)-1); $i>=0; $i--){
                $tampung .= $kata3[$i];
            }
            echo ($kata3 == $tampung )?'true':'false';
            echo "<br>";
        }
       palindrome("civic") ; // true
       palindrome("nababan") ; // true
       palindrome("jambaban"); // false
       palindrome("racecar"); // true


       echo "<br>";
       echo "<h3> Soal No 4 Tentukan Nilai </h3>";
       function tentukan_nilai($nilai){
        if($nilai < 60){
            return 'Kurang <br>';
        }elseif ($nilai<70) {
            return 'Cukup <br>';
        }elseif ($nilai<85) {
            return 'Baik <br>';
        }else{
            return 'Sangat Baik<br>';
        }
       }

       echo tentukan_nilai(98); //Sangat Baik
       echo tentukan_nilai(76); //Baik
       echo tentukan_nilai(67); //Cukup
       echo tentukan_nilai(43); //Kurang

      
    ?>
</body>
</html>